import module01
print(module01.name)
print(module01.lengthof_string("Bikram Bhandari"))
import module01 as mc
print("example of alias",mc.name)
print("example of alias",mc.lengthof_string("Bikram Bhandari"))
      
from module01 import name
print(name)
from module01 import lengthof_string
print(lengthof_string("Bikram Bhandari"))
module01.hello_world.say_hello()
print(module01.hello_world.say_hello())
